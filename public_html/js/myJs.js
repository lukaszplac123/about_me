
let arrowAnimation = true;

$(document).ready(function(){
    $('.parallax').parallax();
    $('.carousel').carousel();
    $('#programming-front .card-content').load('resources/txt/front.html').trigger('load');
    $('#programming-back .card-content').load('resources/txt/back.html').trigger('load');
    //add event listener on scrolling
    window.addEventListener("scroll", onScroll);
    
    loopArrow();
   
});
//animateArrow when scrollTop
function onScroll(){
    let distance = $("body").scrollTop();
    if (distance < 1){
        arrowAnimation = true;
        loopArrow();
    } else{
        arrowAnimation = false;
    }
}
 //end
  var options = [
    {selector: '.first-row', offset: 100, callback: function(el) {
      Materialize.toast("About Me section", 2000 );
      animateAboutMe(el);
    } },
    {selector: '.second-row', offset: 250, callback: function(el) {
      animateProgramming(el);
      Materialize.toast("Programming section", 2000 );
    } },
    {selector: '.third-row', offset: 400, callback: function(el) {
      Materialize.showStaggeredList($(el));
    } },
    {selector: '.fourth-row', offset: 500, callback: function(el) {
      Materialize.fadeInImage($(el));
    } }
    ];
    Materialize.scrollFire(options);
     
//    $('.carousel.carousel-slider').carousel({full_width: true});

    let arrowImage = document.createElement('img');
    let arrowAttributeSource = document.createAttribute('src');
    arrowAttributeSource.value = 'resources/pic/arrow-4-down.png';
    arrowImage.setAttributeNode(arrowAttributeSource);
    $('.arrow-div').append(arrowImage).addClass('arrow-image');



function loopArrow(){
    if (arrowAnimation){
    $('.arrow-div').animate({
        opacity : 1,
        padding: "170 0 0 0",
    }, 1000, function(){
        loopArrowBack();
    });
    }
}

function loopArrowBack(){
        $('.arrow-div').animate({
        opacity : 0.1,
        padding: "0 0 0 0",
    }, 1000, function(){
        loopArrow();
    });
}

function animateAboutMe(el){
    
    $('#about_me .card-content').load('resources/txt/AboutMe.html').css("opacity","0").trigger('load');
    $(el).animate({
        opacity:1
    }, 3000, function(){});
    
    $('.about-me-text').animate({
            opacity: 1
        },1000, function(){
            $('#about_me .card-content').animate({
            opacity: 1
            },800);
        });
    animateTitle(document.getElementsByClassName('about'));
}

function animateProgramming(el){
    $(el).animate({
        opacity:1
    }, 2200, function(){
    });
    animateTitle(document.getElementsByClassName('front'));
    animateTitle(document.getElementsByClassName('back'));

}

function animateTitle(text){
    $(text).animate({
        "font-size": '3rem',
        "opacity" : '0.7'
    },500, function(){
        $(text).animate({
            "font-size":"3.56rem",
            "opacity" : '1'
        },500); 
        });
}



