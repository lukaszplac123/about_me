
let notes = [];

//DOM objects
const noteArea = document.getElementById("note");
const addButton = document.getElementById("addButton");
const veryImportantNote = document.getElementsByClassName("col-md-4 very-important")[0];
const lessImportantNote = document.getElementsByClassName("col-md-4 less-important")[0];
const leastImportantNote = document.getElementsByClassName("col-md-4 least-important")[0];
const veryImportantButton = document.getElementById("very-important-radio");
const lessImportantButton = document.getElementById("less-important-radio");
const leastImportantButton = document.getElementById("least-important-radio");


//listeners
addButton.addEventListener("click", addNote);


class Note {
    constructor(note, index){
        this.note = note;
        this.index = index;
    }
    refreshContent(){
        let node = document.createElement("div");
        node.setAttribute("class", "well well-sm");
        node.addEventListener("click", () => removeNote(node, this.index));
        console.log(this.index);
        this.placeholder.appendChild(node).innerHTML = this.note;
        //console.log(this.placeholder.innerHTML);
    }
}

class VeryImportantNote extends Note{
    constructor(note, index){
        super(note, index);
        this.placeholder = veryImportantNote;
    }
}


class LessImportantNote extends Note{
    constructor(note, index){
        super(note, index);
        this.placeholder = lessImportantNote;
    }
}


class LeastImportantNote extends Note{
    constructor(note, index){
        super(note, index);
        this.placeholder = leastImportantNote;
    }
}

let mapOfImportance = new Map();
mapOfImportance.set(veryImportantButton, VeryImportantNote);
mapOfImportance.set(lessImportantButton, LessImportantNote);
mapOfImportance.set(leastImportantButton, LeastImportantNote);

function addNote(){

    let radioButton = document.querySelector('input[name="optradio"]:checked');
    const Constructor = mapOfImportance.get(radioButton);
    notes.push(new Constructor(noteArea.value, notes.length));
    //dodaje ostatnia notatke do odpowiedniego kontenera
    notes[notes.length - 1].refreshContent();
    console.log(notes);

}

function removeNote(node, index){
    node.remove();
    notes.splice(index, 1);
    for (let i = index; i < notes.length; i++){
        notes[i].index--;
        console.log(notes[i].index);
    }
}



